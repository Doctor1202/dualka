﻿using Ardalis.Specification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dualca.DataAccess.Interface
{
    public interface IBaseRepository<T>
    {
        IUnitOfWork UnitOfWork { get; }
        Task<T> GetByIdAsync(int id);
        Task<IEnumerable<T>> GetAsync();
        Task<T> InsertASync(T item);
        Task UpdateAsync(T item);
        Task DeleteAsync(int id);
        Task<IEnumerable<T>> GetAsync(ISpecification<T> specification);
        Task<T> GetSingleAsync(ISpecification<T> specification);
    }
}
