﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dualca.DataAccess.Interface
{
    public interface IUnitOfWork
    {
        Task CommitAsync();
    }
}
