﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Dualca.Domain;
using Dualca.DataAccess.Interface;

namespace Dualca.DataAccess
{
    public class DualcaDBContext : DbContext, IUnitOfWork
    {
        public DbSet<User> User { get; set; }
        public DbSet<Group> Group { get; set; }
        public DbSet<Payment> Payment { get; set; }

        public DualcaDBContext(DbContextOptions<DualcaDBContext> options) : base(options) { }

        public async Task CommitAsync()
        {
            await this.SaveChangesAsync(cancellationToken: default);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>(b =>
            {
                b.HasKey(x => x.Id);
                b.HasMany(x => x.Group)
                    .WithMany(u => u.User);
                b.HasIndex(x => x.Name)
                    .IsUnique();
            });

            modelBuilder.Entity<Group>(b =>
            {
                b.HasKey(x => x.Id);
                b.HasMany(x => x.User)
                    .WithMany(u => u.Group);
                b.HasIndex(x => x.Name)
                    .IsUnique();
            });

            /*protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            {
                var conn = @"Data Source=(LocalDb)\MSSQLLocalDB;Initial Catalog=DualcaApp;";
                optionsBuilder.UseSqlServer(conn);
            }*/
        }
    }
}

