using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Dualca.DataAccess;
using Microsoft.EntityFrameworkCore;
using Dualca.DataAccess.Interface;
using Dualca.BusinessLogic.Implementation;
using Dualca.BusinessLogic.Interfaces;
using Dualca.DataAccess.Implementation;

namespace Dualca.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Dualca.API", Version = "v1" });
            });

            services.AddScoped<IUserService, UserService>();
            //services.AddScoped<IGroupService, GroupService>();
            //services.AddScoped<IPaymentService, PaymentService>();
            //services.AddScoped<IExpenceService, ExpenceService>();

            services.AddScoped(typeof(IBaseRepository<>), typeof(BaseRepository<>));

            services.AddDbContext<DualcaDBContext>(otp => otp.UseSqlServer(Configuration.GetConnectionString("DualcaDB")));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Dualca.API v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
