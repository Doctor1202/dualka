﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dualca.API.DTO
{
    public class GroupGetDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<int> User { get; set; }
    }
}
