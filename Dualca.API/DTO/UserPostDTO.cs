﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dualca.API.DTO
{
    public class UserPostDTO
    {
        public string Name { get; set; }
        public List<int> GroupId { get; set; }
    }
}
