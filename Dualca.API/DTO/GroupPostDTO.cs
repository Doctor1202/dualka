﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dualca.API.DTO
{
    public class GroupPostDTO
    {
        public string Name { get; set; }
        public List<int> UsersId { get; set; }
    }
}
