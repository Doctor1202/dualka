﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Dualca.API.DTO;
using Dualca.DataAccess;
using Dualca.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dualca.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class GroupController: Controller
    {
        private readonly DualcaDBContext _context;

        public GroupController(DualcaDBContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult GetGroups()
        {
            var groupsWithUsers = _context.Group.AsNoTracking()
               .Select(group => new GroupGetDTO
               {
                   Id = group.Id,
                   Name = group.Name,
                   User = group.User.Select(g => g.Id).ToList()
               }).ToList();

            return Ok(groupsWithUsers);
        }

        [HttpGet("{id}")]
        public async Task<Group> GetGroup(int id)
        {
        return await  _context.Group.AsNoTracking().SingleAsync(Group => Group.Id == id);
        }

        [HttpPost]
        public async Task<int> PostGroup(GroupPostDTO GroupDTO)
        {
            var Group = new Group { Name = GroupDTO.Name };

            if(GroupDTO.UsersId != null)
            {
                var users = _context.User.Where(user => GroupDTO.UsersId.Contains(user.Id));
                Group.User = await users.ToListAsync();
            }

            await _context.Group.AddAsync(Group);
            await _context.SaveChangesAsync();

            return Group.Id;
        }
        
        [HttpDelete]
        public async Task<string> DeleteGroup(int id)
        {
            var group = _context.Group.Where(x => x.Id == id).SingleOrDefault();
            if (group == null) return "Group not found!";

            _context.Group.Remove(group);
            await _context.SaveChangesAsync();
            return "Group has successfully deleted!";
        }
    }
}
