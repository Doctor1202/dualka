﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Dualca.API.DTO;
using Dualca.DataAccess;
using Dualca.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dualca.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController: Controller
    {
        private readonly DualcaDBContext _context;

        public UserController(DualcaDBContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult GetUsers()
        {
            var usersWithGroups = _context.User.AsNoTracking()
                .Select(user => new UserGetDTO
                {
                    Id = user.Id,
                    Name = user.Name,
                    Group = user.Group.Select(g => g.Id).ToList()
                }).ToList();

            return Ok(usersWithGroups);
        }

        [HttpGet("{id}")]
        public async Task<User> GetUser(int id)
        {
            return await _context.User.AsNoTracking().SingleAsync(User => User.Id == id);
        }

        [HttpPost]
        public async Task<string> PostUser(UserPostDTO UserDto)
        {
            var User = new User { Name = UserDto.Name };
            if (User.Name == "")
            {
                return "Error creating user!";
            }

            if (UserDto.GroupId != null)
            {
                var groups = _context.Group.Where(group => UserDto.GroupId.Contains(group.Id));
                User.Group = await groups.ToListAsync();
            }

            await _context.User.AddAsync(User);
            await _context.SaveChangesAsync();

            return $"User {User.Name} created. User ID: {User.Id}";
        }

        [HttpDelete]
        public async Task<string> DeleteUser(int id)
        {
            var user = _context.User.Where(x => x.Id == id).SingleOrDefault();
            if (user == null) return "User not found!";

            _context.User.Remove(user);
            await _context.SaveChangesAsync();
            return "User has successfully deleted!";
        }
    }
}
