﻿using Dualca.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dualca.BusinessLogic.Interfaces
{
    public interface IExpenceService
    {
        Task<IEnumerable<ExpenceHeader>> GetExpencesAsync();
        Task<ExpenceHeader> GetExpenceByIdAsync(int id);
        Task CreateExpence(ExpenceHeader expenceHeader);

    }
}
