﻿using Dualca.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dualca.BusinessLogic.Interfaces
{
    public interface IUserService
    {
        Task<IEnumerable<User>> GetUsersAsync();
        Task<User> GetUserWithIdAsync(int id);
    }
}
