﻿using System.Net;

namespace Dualca.BusinessLogic.Exceptions
{
    public class GroupNotFoundException : BaseException
    {
        public GroupNotFoundException() : base("Group not found", HttpStatusCode.NotFound) { }
    }
}
