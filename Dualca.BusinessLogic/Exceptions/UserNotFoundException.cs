﻿using System.Net;

namespace Dualca.BusinessLogic.Exceptions
{
    public class UserNotFoundException : BaseException
    {
        public UserNotFoundException() : base("User not found!", HttpStatusCode.NotFound) { }
    }
}
