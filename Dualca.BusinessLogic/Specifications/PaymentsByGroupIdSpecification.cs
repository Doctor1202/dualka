﻿using Ardalis.Specification;
using Dualca.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dualca.BusinessLogic.Specifications
{
    public class PaymentsByGroupIdSpecification : Specification<Payment>
    {
        public PaymentsByGroupIdSpecification(int id)
        {
            Query.Include(x => x.Group.Id == id);
        }

    }
}
