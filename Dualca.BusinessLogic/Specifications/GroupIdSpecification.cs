﻿using Ardalis.Specification;
using Dualca.Domain;
using System.Linq;

namespace Dualca.BusinessLogic.Specifications
{
    public class GroupIdSpecification : Specification<User>
    {
        public GroupIdSpecification(int id)
        {
            Query.Where(x => x.Id == id);
        }

    }
}
