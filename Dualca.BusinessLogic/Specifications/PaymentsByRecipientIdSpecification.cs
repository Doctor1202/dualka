﻿using Ardalis.Specification;
using Dualca.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dualca.BusinessLogic.Specifications
{
    public class PaymentsByRecipientIdSpecification : Specification<Payment>
    {
        public PaymentsByRecipientIdSpecification(int id)
        {
            Query.Include(x => x.Recipient.Id == id);
        }
    }
}
