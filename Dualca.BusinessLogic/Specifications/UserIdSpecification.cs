﻿using Ardalis.Specification;
using Dualca.Domain;
using System.Linq;

namespace Dualca.BusinessLogic.Specifications
{
    public class UserIdSpecification : Specification<User>
    {
        public UserIdSpecification(int id)
        {
            Query.Where(x => x.Id == id);
        }

    }
}
