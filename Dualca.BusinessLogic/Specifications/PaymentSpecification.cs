﻿using Ardalis.Specification;
using Dualca.Domain;
using System.Linq;

namespace Dualca.BusinessLogic.Specifications
{
    public class PaymentSpecification : Specification<Payment>
    {
        public PaymentSpecification(int id)
        {
            Query.Where(x => x.Id == id);
        }

    }
}
