﻿using Ardalis.Specification;
using Dualca.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dualca.BusinessLogic.Specifications
{
    public class ExpenceByIdSpecification : Specification<ExpenceHeader>
    {
        public ExpenceByIdSpecification(int id)
        {
            Query.Where(x => x.Id == id)
                .Include(y => y.ExpenceDetails);
        }

    }
}
