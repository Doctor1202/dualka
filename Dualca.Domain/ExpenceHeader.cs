﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dualca.Domain
{
    public class ExpenceHeader
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public string UserId { get; set; }
        
        public User User { get; set; }

        public int GroupId { get; set; }

        public Group Group { get; set; }
        public List<ExpenceDetail> ExpenceDetails { get; set; }
    }
}
