﻿using Dualca.Domain.Interface;
using System.Collections.Generic;

namespace Dualca.Domain
{
    public class Group : IEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public List<User> User { get; set; }

        public List<Payment> Payment { get; set; }
    }
}
