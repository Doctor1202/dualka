﻿namespace Dualca.Domain.Interface
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}
