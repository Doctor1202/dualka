﻿using Dualca.Domain.Interface;
using System.Collections.Generic;

namespace Dualca.Domain
{
    public class User : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public List<Group> Group { get; set; }
    }
}
